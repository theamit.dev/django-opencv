import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()
setuptools.setup(
    name='dajngo-opencv',
    version='0.3',
    # scripts=['dokr'],
    author='Amit Anand',
    author_email='onewaytoconnect@gmail.com',
    description="Django Opencv integratio",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/amitanand/django-opencv",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
